#ifndef correctionWrapper_
#define correctionWrapper_

#include "correction.h"
#include <math.h> 

class MyCorrections {
  public:
    MyCorrections();
    MyCorrections(std::string filename, std::string correction_name);  
    double eval(const std::vector<correction::Variable::Type>& values);

  private:
    correction::Correction::Ref SF_;
};

#endif